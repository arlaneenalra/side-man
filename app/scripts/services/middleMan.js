'use strict';
/* global io:false */

angular.module('middleManServices', [ 'btford.socket-io' ])
  .factory('middleManSocket', [ 'socketFactory',
    function (socketFactory) {
      console.log('connecting');
      var socket = io.connect('http://localhost:9090');
      var wrappedSocket = socketFactory({
        ioSocket: socket
      });

      wrappedSocket.forward('request');
      wrappedSocket.forward('setup');

      return wrappedSocket;
    }
  ]);
