'use strict';

angular.module('sideManApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'treeControl',
  'sideManControllers',
  'sideManDirectives'
])
.config(function ($routeProvider) {
  $routeProvider
    .when('/', {
      templateUrl: 'views/main.html',
      controller: 'MainCtrl'
    })
    .otherwise({
      redirectTo: '/'
    });
  
});

