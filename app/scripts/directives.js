'use strict';

angular.module('sideManDirectives', [])
.directive('scrollBox', [ '$window', function ($window) {
  return {
    link: function (scope, elem, attrs) {

      var setHeight = function () {
        var par = elem.parent(),
          height = par.height();

        // find height available to this element        
        par.children().each (function (idx, child) {
          child = $(child);
          if (!child.is(elem)) {
            height -= child.outerHeight();
          }
        });

        elem.height(height);
      };

      setHeight();
      angular.element($window).bind('resize', setHeight);
    }
  };
}]);

