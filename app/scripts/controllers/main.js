'use strict';

// container class for request data
var NestedTree = function (part, data) {
  part = part || new Date();
  
  // setup the public components
  this.children = {};
  this.view = {
    pathPart: part,
    children: [],
  };

  if (data) {
    this.view.data = data; 
  }
};

// blow away all data
NestedTree.prototype.clear = function () {
  this.children = {};
  this.view.children.length = 0;
};

// return the child or attach a new one as needed
NestedTree.prototype.getChild = function (part, data) {
  var children = this.children;

  // if we have the child, return it
  if (children[part]) {
    return children[part];
  }

  // if not, add a new one to the view and the lookup
  // list of children.
  return this.logData(part, data);
};

// Log a request at a given point in the tree
NestedTree.prototype.logData = function (part, data) {
  var view = this.view,
    children = this.children,
    retVal = new NestedTree(part, data);

  // The data represents a leaf node request if there
  // part is falsy
  if (part) {
    this.children[part] = retVal;
  }

  // Add the new child tree element to our view
  // and out lookup list. That way we can append to the 
  // tree quickly without having to regenerate it.
  view.children.push(retVal.view);
 
  return retVal;
}

angular.module('sideManControllers', ['middleManServices'])
  .controller('MainCtrl',
    function ($scope, middleManSocket) {
      var requestTree = new NestedTree(),
        parser = document.createElement('a');

      // attach the view
      $scope.viewTree = requestTree.view;

      // setup tree view options
      $scope.treeOptions = {
        dirSelectable: false/*,
        injectClasses: {
          ul: 'list-group',
          li: 'list-group-item'
        }*/
      };

      $scope.clearRequests = function () {
        console.log('Called');
        requestTree.clear();
      }

      // Just log the setup
      $scope.$on('socket:setup', function (ev, data) {
        console.log(ev);
        console.log(data);
      });

      $scope.$on('socket:request', function (ev, data) {
        parser.href = data.url;
        var host = parser.protocol + '//' + parser.hostname,
          pathParts = (parser.pathname || '').split('/').slice(1),
          ptr = requestTree.getChild(host);

        // if the path ended in a / drop the empty string we 
        // get from split
        if (pathParts[pathParts.length - 1] === '') {
          pathParts.length--;
        }

        // walk into the tree until we find the right spot
        pathParts.forEach(function (part) {
          ptr = ptr.getChild(part); 
        });

        // add search parsed out search and hash strings to list
        data.search = parser.search;
        data.hash = parser.hash;

        // / can't be part of the above keys
        ptr.logData(false, data);
      });
      
    });
